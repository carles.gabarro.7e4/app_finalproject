package com.example.app_finalproject.android.start

import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.app_finalproject.android.R

class StartFragment: Fragment(R.layout.start_layout) {

    private lateinit var loginButton: Button
    private lateinit var registerButton: Button

//    private lateinit var sp: SharedPreferences

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        loginButton = view.findViewById(R.id.start_login)
        registerButton = view.findViewById(R.id.start_register)

        loginButton.setOnClickListener {
//            if (sp.getString("username", "").toString() != "" && sp.getString("password", "").toString() != ""){

//            } else {
                Navigation.findNavController(view).navigate(R.id.action_startFragment_to_loginFragment)
//            }
        }

        registerButton.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_startFragment_to_registerFragment)
        }
    }
}
