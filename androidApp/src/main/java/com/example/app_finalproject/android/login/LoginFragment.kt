package com.example.app_finalproject.android.login

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.example.app_finalproject.android.R
import com.google.android.material.textfield.TextInputEditText


class LoginFragment: Fragment(R.layout.login_layout) {

    private lateinit var userText: TextInputEditText
    private lateinit var passText: TextInputEditText
    private lateinit var loginButton: Button

    private lateinit var remeberCheck: CheckBox

    //private lateinit var sp: SharedPreferences

    private val viewModel: LoginViewModel by activityViewModels()

    @SuppressLint("WrongConstant", "ShowToast")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        sp = PreferenceManager.getDefaultSharedPreferencesName(context)
        setUI(view)

        loginButton.setOnClickListener{
            for(i in viewModel.getUsers().indices){
                if (userText.text.toString() == viewModel.getUsers()[i].username
                    && passText.text.toString() == viewModel.getUsers()[i].password){
                    Toast.makeText(context, "GOOD LOGIN", 200).show()
//                    if (remeberCheck.isActivated) {
//                        sp.edit().putString("username", userText.toString()).apply()
//                        sp.edit().putString("password", passText.toString()).apply()
//                    }
                    Navigation.findNavController(view).navigate(R.id.action_loginFragment_to_mainFragment)
                }
            }
        }
    }

    //FUNCTIONS
    private fun setUI (view: View){
        userText = view.findViewById(R.id.username_text)
        passText = view.findViewById(R.id.password_text)
        loginButton = view.findViewById(R.id.login_button)
        remeberCheck = view.findViewById(R.id.save_check)
    }

}
