package com.example.app_finalproject.android.user

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.example.app_finalproject.android.R
import com.google.android.material.floatingactionbutton.FloatingActionButton

class UserFragment: Fragment(R.layout.user_layout) {

    private lateinit var urImage: ImageView
    private lateinit var urName: TextView
    private lateinit var urRecyclerView: RecyclerView
    private lateinit var urAddProd: FloatingActionButton


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUI(view)

        urAddProd.setOnClickListener {

        }
    }

    //FUNCTIONS
    private fun setUI(view: View){
        urImage = view.findViewById(R.id.user_image)
        urName = view.findViewById(R.id.ur_name_text)
        urRecyclerView = view.findViewById(R.id.user_prod_recycler)
        urAddProd = view.findViewById(R.id.ur_add_button)
    }
}
