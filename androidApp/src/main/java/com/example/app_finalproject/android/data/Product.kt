package com.example.app_finalproject.android.data

data class Product(
    var id: Int,
    var name: String,
    var image: String,
    var price: Double,
)
