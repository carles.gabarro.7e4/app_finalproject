package com.example.app_finalproject.android.register

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.example.app_finalproject.android.R
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth

class RegisterFragment: Fragment(R.layout.register_layout) {

    private lateinit var firstName: TextInputEditText
    private lateinit var email: TextInputEditText
    private lateinit var username: TextInputEditText
    private lateinit var password: TextInputEditText
    private lateinit var regButton: Button

    private val viewModel: RegisterViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUI(view)

        regButton.setOnClickListener {
            if (firstName.text.toString() != "" && email.text.toString() != "" && username.text.toString() != "" && password.text.toString() != "") {
                viewModel.addUser(firstName.toString(), email.toString(), username.toString(), password.toString() )
//                FirebaseAuth.getInstance().
//                createUserWithEmailAndPassword(email.toString(), password.toString())
//                    .addOnCompleteListener {
//                        if(it.isSuccessful){
//                            val emailLogged = it.result?.user?.email
//                            val direction = RegisterFragmentDirections.actionRegisterFragmentToLoginFragment(emailLogged!!)
//                            Navigation.findNavController(view).navigate(direction)
//                        }
//                        else{
//                            Toast.makeText(context, "Error to register your User", Toast.LENGTH_SHORT).show()
//                        }
//                    }
            }
            Navigation.findNavController(view).navigate(R.id.action_registerFragment_to_loginFragment)
        }

    }

    //FUNCTIONS
    private fun setUI(view: View){
        firstName = view.findViewById(R.id.firstName_text)
        email = view.findViewById(R.id.email_text)
        username = view.findViewById(R.id.username_text_reg)
        password = view.findViewById(R.id.password_text_reg)
        regButton = view.findViewById(R.id.register_button)
    }

}
