package com.example.app_finalproject.android.data

data class User(
    var id: Int,
    var firstName: String,
    var email: String,
    var username: String,
    var password: String,
    var products: List<Product>
)