package com.example.app_finalproject.android.register

import androidx.lifecycle.ViewModel
import com.example.app_finalproject.android.data.User

class RegisterViewModel: ViewModel() {

    var user = mutableListOf<User>()

    fun addUser(firstName: String, email: String, username: String, password: String ) {
        user.add(User(
            user.size+1,
            firstName,
            email,
            username,
            password,
            emptyList())
        )
    }

}
