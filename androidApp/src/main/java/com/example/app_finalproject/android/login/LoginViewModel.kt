package com.example.app_finalproject.android.login

import androidx.lifecycle.ViewModel
import com.example.app_finalproject.android.data.User

class LoginViewModel: ViewModel() {

    var user = mutableListOf<User>()

    init {
        user.add(User(
            1,
            "Carles",
            "carlesgaba24@gmail.com",
            "charly24",
            "ch24",
            emptyList()
        ))
    }

    fun getUsers() = user
}
